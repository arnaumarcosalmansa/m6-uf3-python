import bson
import pymongo
from pymongo import MongoClient
import datetime
from random import randint

client = MongoClient()
db = client["project"]

'''
    FUNCIÓN QUE DETERMINA LA ACCIÓN A REALIZAR
'''


def command(command, args):
    if(command == "type"):
        searchByType(args[0])
    elif(command == "search"):
        result = search(args[0])
        if(len(args) >= 2):
            if(args[1] == "evol"):
                result = listEvols(result)
            else:
                result = limitFields(result, args[1:])
                result = dictToTuple(result)
            print(result)
        else:
            print("Tienes que poner parametros de busqueda.")
    elif(command == "catch"):
        catch(args[0])
        team_size()
    elif(command == "release"):
        release(args[0])
        team_size()
    elif(command == "candy"):
        candy(args[0])
    elif(command == "type"):
        searchByType(args[0])


'''
    FUNCIÓN QUE BUSCA UN POKEMON POR SU NOMBRE O NUMERO
'''


def search(nameOrNum, proj={}):
    global db
    coll = db["pokemon"]

    query = {"num": nameOrNum} if nameOrNum.isdigit() else {"name": nameOrNum}

    return (coll.find_one(query, proj) if bool(proj) else coll.find_one(query))


'''
    FUNCIÓN QUE FILTRA UN DICCIONARIO (POKEMON) Y DEJA SOLO LOS CAMPOS ESPECIFICADOS
'''


def limitFields(dic, fields):
    result = {'name': dic['name']}
    for field in fields:
        result[field] = dic[field]
    return result


'''
    FUNCIÓN QUE CONVIERTE LOS VALORES DE UN DICCIONARIO A UNA TUPLA
'''


def dictToTuple(dic):
    return tuple(dic.values())


'''
    FUNCIÓN QUE LISTA LA LÍNEA EVOLUTIVA DE UN POKEMON
'''


def listEvols(pokemon):
    evols = []

    if "prev_evolution" in pokemon:
        evols.extend(list(map(namef, pokemon["prev_evolution"])))

    evols.append(pokemon["name"])

    if "next_evolution" in pokemon:
        evols.extend(list(map(namef, pokemon["next_evolution"])))

    return evols

# PARA COGER EL NOMBRE DE UN POKEMON


def namef(x):
    return x["name"]


'''
    Función que captura un pokémon.
'''


def catch(nameOrNum, verbose=True, CP=randint(1, 500), catch_date=datetime.datetime.now()):
    global db
    coll = db["team"]

    proj = {
        '_id': 0,
        'num': 1,
        'name': 1,
        'catch_date': 1,
        'CP': 1,
        'candy': 1,
        'candy_count': 1,
        'current_candy': 1,
        'next_evolution': 1
    }

    pokemon = search(nameOrNum, proj=proj)
    key = {'num': nameOrNum} if nameOrNum.isdigit() else {'name': nameOrNum}

    if coll.count_documents(key) > 0:
        if(verbose):
            print("Ja tens aquest pokemon.", end=' ')
    else:
        pokemon['catch_date'] = catch_date
        pokemon['CP'] = CP
        pokemon['current_candy'] = 0

        coll.insert_one(pokemon)

        if(verbose):
            print(pokemon['name'], "atrapat.", end=' ')


'''
    Función que libera un pokémon si lo tienes.
'''


def release(nameOrNum, verbose=True):
    global db
    coll = db["team"]

    key = {'num': nameOrNum} if nameOrNum.isdigit() else {'name': nameOrNum}
    delete_results = coll.delete_many(key)

    if(delete_results.deleted_count == 0):
        if(verbose):
            print("No tens aquest pokemon.", end=' ')
    else:
        if(verbose):
            print("Lliberat.", end=' ')


'''
    Función que muestra el nombre de pokemons que tienes en la colección.
'''


def team_size():
    global db
    coll = db["team"]

    count = coll.count_documents({})

    print("Nombre de pokémon: ", count)


'''
    Función que incrementa el número de caramelos de un pokemon.
    Si el número de caramelos actuales iguala al número de caramelos necesarios
    el pokémon evoluciona.
'''


def candy(nameOrNum):
    global db
    team = db["team"]

    key = {'num': nameOrNum} if nameOrNum.isdigit() else {'name': nameOrNum}
    result = team.find_one_and_update(
        key, {"$inc": {"current_candy": 1}}, new=True)

    if result is None:
        print("No tens aquest pokemon")
    elif('candy_count' in result and result['current_candy'] >= result['candy_count']):
        evolve(result['name'])
    else:
        print("Caramel donat a", result['name'] +
              ". Caramels ", result['current_candy'])


'''
    Evoluciona un pokémon al siguiente de su línea evolutiva.
'''


def evolve(name):
    global db
    team = db["team"]

    key = {'candy_count': {'$exists': True}, 'name': name}
    proj = {"next_evolution": 1, "CP": 1, "catch_date": 1}
    found = team.find_one(key, proj)

    cp = found["CP"] + 100
    catch_date = found["catch_date"]

    next_evol = found['next_evolution'][0]
    next_evol_num = next_evol['num']
    next_evol_name = next_evol['name']

    release(name, verbose=False)
    catch(next_evol_num, verbose=False, CP=cp, catch_date=catch_date)

    print(name, "evoluciona a", next_evol_name)


'''
    Busca todos los pokémons que tienen un tipo
'''

#aggregation framework?
def searchByType(type):
    global db
    coll = db["pokemon"]

    query = {"type": type}
    pokimons = coll.find(query, {"_id": 0, "name": 1})
    pokimonscount = coll.count_documents(query)

    names = list(map(namef, pokimons))

    print(names, pokimonscount)

    #OTRA FORMA EN EL ARCHIVO TESTS



# MAIN
while True:
    line = input().split()
    if(len(line) > 0):
        command(line[0].lower(), line[1:])
    else:
        break
