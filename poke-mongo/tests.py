import bson
import pymongo
from pymongo import MongoClient

#print(list(map(int, ['1', '2', '3'])))

#EL EJERCICIO 4 HECHO DE UNA FORMA INNECESARIAMENTE COMPLICADA
#SOLO PARA USAR AGGREGATION FRAMEWORK

def namef(x):
    return x["name"]

client = MongoClient()
db = client["project"]

coll = db['pokemon']

#EL TIPO
tipo = input("Tipo: ")
#EL TIPO

query = [
    {"$unwind" : "$type"},
    {"$match" : {"type" : tipo}}
    ]

res = coll.aggregate(query)
res = list(map(namef, res))

querycount = [
    {"$unwind" : "$type"},
    {"$match" : {"type" : tipo}},
    {"$group": {"_id": "$type", "count": {"$sum": 1}}}
]
countres = coll.aggregate(querycount)
count = 0
for i in countres:
    count = i['count']
print(res, count)

