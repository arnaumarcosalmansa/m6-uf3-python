#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <windows.h>

using namespace std;

class Position
{
    public:
        int x = 0;
        int y = 0;
};

class Grid
{
    public:
        int width = 0;
        int height = 0;
        char** grid;
    Grid(int width, int height)
    {
        this->width = width;
        this->height = height;

        grid = new (nothrow) char*[this->height];

        for(int i = 0; i < this->width; i++)
        {
            grid[i] = new char[this->height + 1];
            for(int j = 0; j < this->height; j++)
            {
                grid[i][j] = ' ';
            }
            grid[i][this->height] = 0;
        }
    }

    void draw(Position pos, char c)
    {
        this->grid[pos.x][pos.y] = c;
    }
};

int main()
{
    Grid grid = *(new Grid(20, 20));
    Position pos = *(new Position());

    while(true)
    {
        grid.draw(pos, ' ');

        if(GetAsyncKeyState(VK_DOWN) && pos.x < grid.width)
        {
            pos.x++;
        }
        else if(pos.x > 0)
        {
            pos.x--;
        }

        if(GetAsyncKeyState(VK_RIGHT) && pos.y < grid.height)
        {
            pos.y++;
        }
        else if(pos.y > 0)
        {
            pos.y--;
        }

        grid.draw(pos, 'X');

        system("cls");

        char* output = new char[pos.x + 1];

        for(int i = 0; i < pos.x; i++)
        {
            output[i] = ' ';
        }

        for(int i = 0; i < grid.height; i++)
        {
            cout << grid.grid[i] << endl;
        }

        Sleep(100);
    }


    return 0;
}


